from flask import Flask, current_app

app = Flask(__name__)

@app.route('/')
def show_info():
    return 'Test Environment', 200

@app.route('/report.html')
def report():
   return current_app.send_static_file('report.html')

@app.route('/log.html')
def log():
   return current_app.send_static_file('log.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
