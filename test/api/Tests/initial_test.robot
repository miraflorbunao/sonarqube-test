*** Settings ***
Resource    ../Resources/Api.robot
Test Setup    Create New Session

*** Test Cases ***

API: GET /
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  ${response}=    GET On Session    current_session    /    headers=${headers}    expected_status=any
  Status Should Be    200    ${response}
  Should be equal as strings    ${response.text}    User API v1.0.8

API: POST /user (Success)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test1
  ${response}=    POST On Session    current_session    user    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    200    ${response}

API: POST /user (Bad request: Already taken)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test1
  ${response}=    POST On Session    current_session    user    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    400    ${response}

API: POST /user (Bad request: With characters that are not allowed)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test1)
  ${response}=    POST On Session    current_session    user    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    400    ${response}

API: GET /user/<username> (Empty)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  ${response}=    GET On Session    current_session    user/test    headers=${headers}    expected_status=any
  Status Should Be    200    ${response}
  Should be equal as strings    ${response.json()}    {}

API: GET /user/<username> (With match)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  ${response}=    GET On Session    current_session    user/test1    headers=${headers}    expected_status=any
  Status Should Be    200    ${response}
  Should not be equal as strings    ${response.json()}    {}

API: PUT /user/<username> (Not found)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test2
  ${response}=    PUT On Session    current_session    user/non-existent_user    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    404    ${response}

API: PUT /user/<username> (Bad request: Already Taken)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test1
  ${response}=    PUT On Session    current_session    user/test1    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    400    ${response}

API: PUT /user/<username> (Bad request: With characters that are not allowed)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test1)
  ${response}=    PUT On Session    current_session    user/test1    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    400    ${response}

API: PUT /user/<username> (Success)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  &{data}    Create dictionary    username=test2
  ${response}=    PUT On Session    current_session    user/test1    headers=${headers}    expected_status=any    json=${data}
  Status Should Be    200    ${response}

API: DELETE /user/<username> (Not found)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  ${response}=    DELETE On Session    current_session    user/non-existent_user    headers=${headers}    expected_status=any
  Status Should Be    404    ${response}

API: DELETE /user/<username> (Success)
  ${headers}=    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8
  ${response}=    DELETE On Session    current_session    user/test2    headers=${headers}    expected_status=any
  Status Should Be    200    ${response}
