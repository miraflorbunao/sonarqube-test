from flask import Flask, request, jsonify
import pymongo, os, json, re


app = Flask(__name__)
host = os.environ.get('MONGODB_HOST', 'db')
db_user = os.environ.get('MONGODB_USER', 'root')
db_pass = os.environ.get('MONGODB_PASS', 'example')
client = pymongo.MongoClient('mongodb://{}:{}@{}:27017/'.format(db_user, db_pass, host))
db = client.user

@app.route('/')
def api_info():
    return 'User API v1.0', 200

@app.route('/user/<username>', methods=['GET', 'PUT', 'DELETE'])
def process_user_by_username(username):
    user = db.user.find_one({'username': username})
    if request.method == 'GET':
        if user is None:
            return jsonify({}), 200
        else:
            return jsonify({'_id': str(user['_id']), 'username': user['username']}), 200
    elif request.method == 'PUT':
        if user is None:
            return '`{}` is not found'.format(username), 404
        else:
            data = request.json
            if re.match("^[A-Za-z0-9_-]*$", data['username']):
                if db.user.find_one({'username': data['username']}) is None:
                    db.user.update_one(
                        {'username': username},
                        {'$set':{'username': data['username']}}
                    )
                    return 'Username has been updated from `{}` to `{}`'.format(username, data['username']), 200
                return 'Bad Request: `{}` is already taken'.format(data['username']), 400
            return 'Bad Request: The only characters allowed are letters, numbers, hyphens and underscores.', 400
    elif request.method == 'DELETE':
        if user is None:
            return '`{}` is not found'.format(username), 404
        else:
            db.user.delete_one({
                'username': username
            })
            return '`{}` has been deleted'.format(username), 200
    else:
        return '`{}` method is not allowed in this endpoint'.format(request.method), 404

@app.route('/user', methods=['POST'])
def create_user():
    data = request.json
    if re.match("^[A-Za-z0-9_-]*$", data['username']):
        if db.user.find_one({'username': data['username']}) is None:
            inserted_data = db.user.insert_one({
                'username': data['username']
            })
            return 'A new user is created. id: `{}`'.format(str(inserted_data.inserted_id)), 200
        return 'Bad Request: `{}` is already taken'.format(data['username']), 400
    return 'Bad Request: The only characters allowed are letters, numbers, hyphens and underscores.', 400

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
